#![deny(clippy::pedantic)]
use structopt::StructOpt;

fn main() {
    let program = Program::new();

    program.run();
}

#[derive(StructOpt)]
#[structopt(about = "Little tool to convert any string in an binary display.")]
struct Program {
    #[structopt(short, long)]
    phrase: String,
}

impl Program {
    pub fn new() -> Program {
        Program::from_args()
    }

    pub fn phrase(&self) -> String {
        self.phrase.clone()
    }

    pub fn run(self) {
        for b in self.phrase().as_bytes() {
            print!("{:b} ", b);
        }
    }
}